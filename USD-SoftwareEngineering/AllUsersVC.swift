//
//  AllUsersVC.swift
//  USD-SoftwareEngineering
//
//  Created by Lily Hoffman on 9/19/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//
// VC for displaying list of all users (name + major)

import Foundation
import UIKit

class AllUsersVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    //MARK: Local vars
    var users = [UserData]()
    
    //MARK: Override
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        DataService.singleton.observeUsers(completed: { (users) in
            self.users = users
            self.tableView.reloadData()
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ViewUser"{
            let destVC = segue.destination as! ProfileVC
            destVC.userUID = sender as! String
        }
    }
    
    //MARK: Interface Builder
    @IBOutlet var tableView: UITableView!
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath)
        
        cell.textLabel?.text = users[indexPath.row].name
        cell.detailTextLabel?.text = users[indexPath.row].major.joined(separator: " & ")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ViewUser", sender: users[indexPath.row].userUID )
    }
    

    
}
