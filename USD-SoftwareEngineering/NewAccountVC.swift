//
//  NewAccountVC.swift
//  USD-SoftwareEngineering
//
//  Created by Lily Hoffman on 9/19/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//
// VC for creating a new account.
// Asks user for: first name, last name, email, password and allows them to select multiple majors from a list of majors offered at USD
// Authenticates user via Firebase Auth
// Adds user to Firebase Database

import Foundation
import UIKit
import Firebase

class NewAccountVC: UIViewController, PopUpSenderDelegate{
    
    //MARK: PopUpSenderDelegate
    func dataTransfer(majors: [PopUpSelectionData]) {
        self.selectedMajors = majors
    }
    var selectedMajors = [PopUpSelectionData]()

    //MARK: override
    override func viewDidLoad() {
        super.viewDidLoad()
        selectMajorsButton.layer.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5).cgColor
        addTapToDismissKeyboard()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectMajors"{
            let destVC = segue.destination as! MajorSelectionVC
            destVC.delegate = self
            destVC.presentingVC = self
            destVC.dataToDisplay = GlobalActions.majorsList
        }
    }
    
    //MARK: Interface Builder
    @IBOutlet var firstNameTextField: RoundedTextField!
    @IBOutlet var lastNameTextField: RoundedTextField!
    @IBOutlet var emailTextField: RoundedTextField!
    @IBOutlet var passwordTextField: RoundedTextField!
    @IBOutlet var selectMajorsButton: UIButton!
    @IBAction func selectMajorsButtonTapped(_ sender: Any) {
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.regular))
        
        blurEffectView.frame = self.view.frame
        
        self.view.insertSubview(blurEffectView, aboveSubview: firstNameTextField)
        performSegue(withIdentifier: "SelectMajors", sender: nil)
    }

    @IBAction func createAccountButtonTapped(_ sender: Any) {
        
        var selectedMajorStringArray = [String]()
        for selectedMajor in selectedMajors{
            selectedMajorStringArray.append(selectedMajor.title)
        }
        
        //Authenticate user in firebase
        AuthService.singleton.createAccount(sender: self, email: emailTextField.text!, password: passwordTextField.text!){
            
            //Grab newly created user's auth id
            let firebaseUser: User = Auth.auth().currentUser!
            
            //Create new user object
            let newUser = UserData(userUID: firebaseUser.uid, firstName: self.firstNameTextField.text!, lastName: self.lastNameTextField.text!, email: self.emailTextField.text!, major: selectedMajorStringArray, bio: "About me: ")
            
            //Add new user to firebase database
            DataService.singleton.submitNewUser(user: newUser){
                self.performSegue(withIdentifier: "UserCreated", sender: nil)
            }
        }
    }

    //Add tap to dismiss keyboard functionality
    func addTapToDismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }

}
