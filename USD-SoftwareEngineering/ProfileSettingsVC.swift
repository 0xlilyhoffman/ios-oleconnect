//
//  ProfileSettingsVC.swift
//  USD-SoftwareEngineering
//
//  Created by Lily Hoffman on 9/19/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//
// VC for allowing a user to change profile information (name, email, majors)

import Foundation
import UIKit

class UserProfileSettingsVC: UIViewController{
    //MARK: Local Vars
    var userUID: String!
    var user: UserData!
    
    //MARK: Override
    override func viewDidLoad() {
        super.viewDidLoad()
        userLastNameTextField.text = user.name
        userEmailTextField.text = user.email
        userBioTextView.text = user.bio
        
        addTapToDismissKeyboard()
    }
    
    //MARK: Interface Builder
    @IBOutlet var userProfileImage: CircleView!
    @IBOutlet var userLastNameTextField: RoundedTextField!
    @IBOutlet var userMajorButton: RoundedButton!
    @IBAction func userMajorButtonPressed(_ sender: Any) {
        //TODO: allow a user to change their major
    }
    @IBOutlet var userEmailTextField: RoundedTextField!
    @IBOutlet var userBioTextView: RoundedTextView!
    @IBAction func signOutButtonPressed(_ sender: Any) {
        AuthService.singleton.signOut(sender: self)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func submitButtonPressed(_ sender: Any) {
        var name = userLastNameTextField.text
        if name == nil{
            name = user.name
        }
        var email = userEmailTextField.text
        if email == nil{
            email = user.email
        }
        var bio = userBioTextView.text
        if bio == nil{
            bio = user.bio
        }
        DataService.singleton.submitUserUpdate(userUID: userUID, name: name!, email: email!, bio: bio!)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Helper functions
    //Add tap to dismiss keyboard functionality
    func addTapToDismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
