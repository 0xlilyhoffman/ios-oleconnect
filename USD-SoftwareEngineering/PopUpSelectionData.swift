//
//  PopUpSelectionData.swift
//  USD-SoftwareEngineering
//
//  Created by Lily Hoffman on 9/20/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//
// Data model class for representing a list with checkable boxes.
// When a box is checked, selected = true.
// This class is used (1) to display a list of majors to a user when they create an account, and (2) to display a list of majors to a user when they are selecting which roles are required to implement their idea


import Foundation

class PopUpSelectionData{
    //MARK: Private vars
    private var _title: String!
    private var _selected: Bool!
    
    //MARK: public vars
    var title: String{
        get{
            return _title
        }set{
            _title = newValue
        }
    }
    
    var selected: Bool{
        get{
            return _selected
        }set{
            _selected = newValue
        }
    }
    
    //MARK: init
    init(title: String, selected: Bool){
        self.title = title
        self.selected = selected
    }
    
}
