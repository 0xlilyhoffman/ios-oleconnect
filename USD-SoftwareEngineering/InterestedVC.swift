//
//  InterestedVC.swift
//  USD-SoftwareEngineering
//
//  Created by Lily Hoffman on 9/19/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//
// VC for displaying a list of users interested in a particular idea

import Foundation
import UIKit
import Firebase

class InterestedUsersVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    //MARK: Local Vars
    var interestedUsers: [UserData]! //passed in via segue
    var ideaReference: String!      //passed in via segue
    var firebaseUser: User!

    //MARK: Override
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        rootView.layer.cornerRadius = 15.0
        rootView.layer.masksToBounds = true
        
        firebaseUser = Auth.auth().currentUser
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ViewUser"{
            let destVC = segue.destination as! ProfileVC
            destVC.userUID = sender as! String
        }
    }
    
    //MARK: InterfaceBuilder
    @IBOutlet var rootView: UIView!
    @IBOutlet var tableView: UITableView!
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interestedUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath)
        
        cell.textLabel?.text = interestedUsers[indexPath.row].name
        cell.detailTextLabel?.text = interestedUsers[indexPath.row].major.joined(separator: " & ")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ViewUser", sender: interestedUsers[indexPath.row].userUID )
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            if(interestedUsers[indexPath.row].userUID == firebaseUser.uid){
                DataService.singleton.deleteUserFromInterestedList(ideaReference: ideaReference, userReference: interestedUsers[indexPath.row].userUID )
                self.interestedUsers.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
        }
    }
}
