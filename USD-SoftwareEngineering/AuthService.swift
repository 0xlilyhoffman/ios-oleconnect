//
//  AuthService.swift
//  USD-SoftwareEngineering
//
//  Created by Lily Hoffman on 9/20/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//
// AuthService singleton class for interacting with Firebase Authentication

import Foundation
import Firebase


class AuthService{
    //MARK: AuthService singleton
    private static let _singleton = AuthService()
    static var singleton: AuthService{
        return _singleton
    }
    
    //MARK: FirebaseAuth functions
    //Authenitcates a new user via email/password through Firebase Auth
    func createAccount(sender: UIViewController, email: String, password: String, completed: @escaping () -> ()){
        Auth.auth().createUser(withEmail: email, password: password, completion: {(user, error) in
            if error == nil{
                if user != nil{
                    print("no error, user")
                }else{
                    GlobalActions.singleton.displayAlert(sender: sender, title: "Authentication Error", message: "User not authenticated")
                    print("no error, no user")
                }
            }else{
                GlobalActions.singleton.displayAlert(sender: sender, title: "Error on Account Creation", message: "\(error!.localizedDescription)")
                print("error")
                //self.handleFirebaseErrors(error: error!)
            }
            completed()
        })
    }

    //Signs in a user through Firebase Auth
    //On error, prints error via UIAlertController
    //Returns (bool, String) representing (error exists or user does not exist, error message or "success")
    func signIn(sender: UIViewController, email: String, password: String, completed: @escaping (Bool, String) -> ()){
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            if error != nil{
                GlobalActions.singleton.displayAlert(sender: sender, title: "Login Error", message: error!.localizedDescription)
                completed(false, error!.localizedDescription)
            }
            if user == nil{
                GlobalActions.singleton.displayAlert(sender: sender, title: "Login Error", message: "Username / password combination incorrect")
                completed(false, "authentication error")
            }
            if error == nil && user != nil{
                completed(true, "success")
            }
        })
    }
    
    //Sends password reset via FirebaseAuth
    //On error, prints error via UIAlertController
    func forgotPassword(sender: UIViewController, email: String){
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if error != nil{
                GlobalActions.singleton.displayAlert(sender: sender, title: "Password recovery error", message: "Error sending password reset email. Check that your email address is entered correctly in the \"email\" field")
            }
        }
    }
    
    //Signs out a user using Firebase auth
    //On error, prints error via UIAlertController
    func signOut(sender: UIViewController){
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            sender.dismiss(animated: true, completion: nil)
        }catch let signOutError as NSError {
            GlobalActions.singleton.displayAlert(sender: sender, title: "Log out Error", message: "\(signOutError.localizedDescription)")
            return
        }
        
    }
}
