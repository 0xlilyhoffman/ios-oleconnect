//
//  MyInterestsVC.swift
//  USD-SoftwareEngineering
//
//  Created by Lily Hoffman on 10/13/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//
// VC for displaying a list of ideas that a user is interested in
// This VC is accessible through a user's profile

import Foundation
import UIKit
import Firebase

class MyInterestsVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    //MARK: Local Vars
    var firebaseUser: User!
    var interestingIdeas = [IdeaData]()
    
    //MARK: Override
    override func viewDidLoad() {
        networkImage.clipsToBounds = true
        
        tableView.delegate = self
        tableView.dataSource = self
        
        firebaseUser = Auth.auth().currentUser
        
        DataService.singleton.observeInterestingIdeas(userUID: firebaseUser.uid, completed: { (ideas) in
            self.interestingIdeas = ideas
            self.tableView.reloadData()
        })
        
    }
    
    //MARK: Interface Builder
    @IBOutlet var networkImage: BlurredImage!
    @IBOutlet var tableView: UITableView!
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InterestingIdea", for: indexPath)
        cell.textLabel?.text = interestingIdeas[indexPath.row].ideaTitle
        cell.detailTextLabel?.text = interestingIdeas[indexPath.row].ideaSubtitle
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interestingIdeas.count
    }
    
}
