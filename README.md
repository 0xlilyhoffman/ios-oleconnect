# Ole Connect iOS App

App Demo: https://youtu.be/vCUYx96Ua6Y

This app serves to promote interdisciplinary collaboration among USD students. The app allows university students all disciplines to share their ideas, get feedback on their ideas, and communicate with others who may be interested in bringing their ideas to life. The mission is to "promote the exchange, development and execution of ideas"

The app opens to an �idea feed� where students can browse ideas posted by other students. 
Ideas posted are described in terms of: (1) a catchy title and descriptive subtitle (2) a detailed description of the idea, (3) the team roles and individual skills needed to bring the idea to life, and (4) idea owner and contact information. 

Students can select ideas from the feed and (1) view more details about the idea, (2) click a link to the idea author's profile page, (3) post comments on the idea, and (4) express interest in the idea. 

Each user will have a profile displaying their name, email, a short bio, and ideas that they have contributed to the app. Their profile also provides access to a list of ideas that they have expressed interest in.

Ideally, the app will encourage the inspiration and generation of new ideas in addition to providing an opportunity to implement the proposed ideas.